import matplotlib.pyplot as plt

import numpy as np
import math

import csv


def isPrime(a):
    return all(a % i for i in range(2, a))

def factorize(n):
    factors = []

    p = 2
    while True:
        while n % p == 0 and n > 0:  # while we can divide by smaller number, do so
            factors.append(p)
            n = n / p
        p += 1  # p is not necessary prime, but n%p == 0 only for prime numbers
        if p > n / p:
            break
    if n > 1:
        factors.append(int(n))
    return factors


def calculateLegendre(a, p):
    if a >= p or a < 0:
        return calculateLegendre(a % p, p)
    elif a == 0 or a == 1:
        return a
    elif a == 2:
        if p % 8 == 1 or p % 8 == 7:
            return 1
        else:
            return -1
    elif a == p - 1:
        if p % 4 == 1:
            return 1
        else:
            return -1
    elif p/a == 0:
        return 0
    elif not isPrime(a):
        factors = factorize(a)
        # print(factors) // del
        product = 1
        for pi in factors:
            product *= calculateLegendre(pi, p)
        return product
    else:
        if ((p - 1) / 2) % 2 == 0 or ((a - 1) / 2) % 2 == 0:
            return calculateLegendre(p, a)
        else:
            return (-1) * calculateLegendre(p, a)


def count_4(prime):
    quater_count_num = round(prime/4)   # делим по 4
    quater_limit_arr = []               # границы 4ok
    quater_legandre_counter_arr = []
    for i in range(1, 4):
        quater_limit_arr.append(i*quater_count_num)
    quater_limit_arr.append(prime)

    print(quater_limit_arr)
    result_arr = []
    step_arr = []
    min = 0
    max = 0

    result_arr_plot = [[] for i in range(4)]
    step_arr_plot = [[] for i in range(4)]

    step = 160

    print("Prime = " + str(prime) )

    for quater in range(0, 4):

        result_arr = []
        step_arr = []

        index_step = quater_limit_arr[quater] # 1 четверть

        limit = 0
        if (quater == 3):
            limit = quater_limit_arr[quater] + quater_limit_arr[0] - step
        else:
            limit = quater_limit_arr[quater + 1] - step

        right_average = 0
        while (index_step < limit):
            summ = 0 
            step_arr.append(index_step + step/2)
            for number in range(index_step, index_step + step):
                legandre_symbol = calculateLegendre(number, prime)
                if legandre_symbol == 1:
                    summ += 1
                elif legandre_symbol == -1:
                    summ -= 1
            if summ > max:
                max = summ
            if summ < min:
                min = summ
            result_arr.append(summ)
            index_step += step
        right_average = sum(result_arr)/len(result_arr)

        right_variance = 0
        variance_summ = 0
        for average_num in result_arr:
            variance_summ += pow(average_num - right_average, 2)
        right_variance = math.sqrt(variance_summ/len(result_arr))

        # print("------------------------------------\n")
        index_step = quater_limit_arr[quater] # 1 четверть

        if (quater == 0):
            limit = 0 + step
        else:
            limit = quater_limit_arr[quater-1] + step
        left_result_arr = []

        while (index_step > limit):
            summ = 0
            step_arr.append(index_step - step/2) 
            for number in range(index_step, index_step - step, -1):
                legandre_symbol = calculateLegendre(number, prime)
                if legandre_symbol == 1:
                    summ += 1
                elif legandre_symbol == -1:
                    summ -= 1
            if summ > max:
                max = summ
            if summ < min:
                min = summ
            result_arr.append(summ)
            left_result_arr.append(summ)
            index_step -= step
        left_average = sum(left_result_arr)/len(result_arr)

        left_variance = 0
        variance_summ = 0
        for average_num in left_result_arr:
            variance_summ += pow(average_num - right_average, 2)
        left_variance = math.sqrt(variance_summ/len(left_result_arr))

        result_arr_plot[quater] = result_arr
        step_arr_plot[quater] = step_arr
        
        print(str( quater+1 ) + "p/4: " + str(quater_limit_arr[quater]) )
        print("Average: \t " + "{:10.2f}".format(round(left_average, 3)) + " | " + "{:10.2f}".format(round(right_average, 3)) )
        print("Variance: \t " + "{:10.2f}".format(round(left_variance, 3)) + " | " + "{:10.2f}".format(round(right_variance, 3)) )

###############################################################################

        # print(result_arr_plot[quater])
        # print(step_arr_plot[quater])

    
    # width=5
    # height=5
    # rows = 4
    # cols = 1
    # axes=[]
    # fig=plt.figure()

    # for a in range(rows*cols):
        
    #     axes.append( fig.add_subplot(rows, cols, a+1) )
    #     subplot_title=(str( a+1 ) + "p/4")
    #     axes[-1].set_title(subplot_title)  
    #     x0 = np.array(step_arr_plot[a])
    #     y0 = np.array(result_arr_plot[a])
    #     plt.ylim(min-5, max+5)
    #     plt.bar(x0, y0, 100, facecolor='g')
    #     plt.xticks(())
    #     plt.grid(True)
    # fig.tight_layout()    
    # plt.show()


primes_array = []

with open('./crypto_py/primes.csv', newline='') as file_csv:

    reader = csv.DictReader(file_csv)
    for row in reader:
        primes_array.append( int(row['primes']) )

# print(primes_array)

# count_4(primes_array[0])

for prime in primes_array:
    count_4(prime)

