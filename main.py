#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

try:
    import numpy as np
except ImportError:
    print("numpy is not installed")

try:
    import matplotlib.pyplot as plt
except ImportError:
    print("matplotlib is not installed")

# try:
#     from scipy.special import legendre
# except ImportError:
#     print("scipy is not installed")


MAX_P = 10000000007 

def isPrime(a):
    return all(a % i for i in range(2, a))

def factorize(n):
    factors = []

    p = 2
    while True:
        while n % p == 0 and n > 0:  # while we can divide by smaller number, do so
            factors.append(p)
            n = n / p
        p += 1  # p is not necessary prime, but n%p == 0 only for prime numbers
        if p > n / p:
            break
    if n > 1:
        factors.append(int(n))
    return factors


def calculateLegendre(a, p):
    if a >= p or a < 0:
        return calculateLegendre(a % p, p)
    elif a == 0 or a == 1:
        return a
    elif a == 2:
        if p % 8 == 1 or p % 8 == 7:
            return 1
        else:
            return -1
    elif a == p - 1:
        if p % 4 == 1:
            return 1
        else:
            return -1
    elif p/a == 0:
        return 0
    elif not isPrime(a):
        factors = factorize(a)
        # print(factors) // del
        product = 1
        for pi in factors:
            product *= calculateLegendre(pi, p)
        return product
    else:
        if ((p - 1) / 2) % 2 == 0 or ((a - 1) / 2) % 2 == 0:
            return calculateLegendre(p, a)
        else:
            return (-1) * calculateLegendre(p, a)


def count_quater(prime):
    quater_count_num = round(prime/4)   # делим по четвертям
    quater_limit_arr = []               # границы четвертей
    quater_legandre_counter_arr = []
    for i in range(1, 4):
        quater_limit_arr.append(i*quater_count_num)
        quater_legandre_counter_arr.append(0)
    quater_limit_arr.append(prime)
    quater_legandre_counter_arr.append(0)

    # считаем вычеты в диапазоне [0, р]
    for number in range(0, prime):
        legandre_symbol = calculateLegendre(number, prime)

        index_quater = 0

        if number<=quater_limit_arr[0]:
            index_quater = 0
        elif quater_limit_arr[0]<number & number<=quater_limit_arr[1]:
            index_quater = 1
        elif quater_limit_arr[1]<number & number<=quater_limit_arr[2]:
            index_quater = 2
        elif quater_limit_arr[2]<number & number<=quater_limit_arr[3]:
            index_quater = 3

        if legandre_symbol == 1:
            quater_legandre_counter_arr[index_quater] += 1
        elif legandre_symbol == -1:
            quater_legandre_counter_arr[index_quater] -= 1
        # result_arr.append(legandre_symbol) // для графика

    if (quater_legandre_counter_arr[1] == 0 or quater_legandre_counter_arr[2] == 0):
        str_to_file = str(prime) + ',' + ','.join(str(num) for num in quater_legandre_counter_arr) + "\n"
        print(str_to_file)
    
def count_eight(prime):
    quater_count_num = round(prime/8)   # делим по 8
    quater_limit_arr = []               # границы 8ok
    quater_legandre_counter_arr = []
    for i in range(1, 8):
        quater_limit_arr.append(i*quater_count_num)
        quater_legandre_counter_arr.append(0)
    quater_limit_arr.append(prime)
    quater_legandre_counter_arr.append(0)

    # считаем вычеты в диапазоне [0, р]
    for number in range(0, prime):
        legandre_symbol = calculateLegendre(number, prime)

        index_quater = 0

        if number<=quater_limit_arr[0]:
            index_quater = 0
        elif quater_limit_arr[0]<number & number<=quater_limit_arr[1]:
            index_quater = 1
        elif quater_limit_arr[1]<number & number<=quater_limit_arr[2]:
            index_quater = 2
        elif quater_limit_arr[2]<number & number<=quater_limit_arr[3]:
            index_quater = 3
        elif quater_limit_arr[3]<number & number<=quater_limit_arr[4]:
            index_quater = 4
        elif quater_limit_arr[4]<number & number<=quater_limit_arr[5]:
            index_quater = 5
        elif quater_limit_arr[5]<number & number<=quater_limit_arr[6]:
            index_quater = 6
        elif quater_limit_arr[6]<number & number<=quater_limit_arr[7]:
            index_quater = 7
        

        if legandre_symbol == 1:
            quater_legandre_counter_arr[index_quater] += 1
        elif legandre_symbol == -1:
            quater_legandre_counter_arr[index_quater] -= 1
        # result_arr.append(legandre_symbol) // для графика

    str_to_file = str(prime) + ',' + ','.join(str(num) for num in quater_legandre_counter_arr) + "\n"
    print(str_to_file)

def count_16(prime):
    quater_count_num = round(prime/64)   # делим по 8
    quater_limit_arr = []               # границы 8ok
    quater_legandre_counter_arr = []
    for i in range(1, 64):
        quater_limit_arr.append(i*quater_count_num)
        quater_legandre_counter_arr.append(0)
    quater_limit_arr.append(prime)
    quater_legandre_counter_arr.append(0)

    # считаем вычеты в диапазоне [0, р]
    for number in range(0, prime):
        legandre_symbol = calculateLegendre(number, prime)

        index_quater = 0

        if number<=quater_limit_arr[0]:
            index_quater = 0
        elif quater_limit_arr[0]<number & number<=quater_limit_arr[1]:
            index_quater = 1
        elif quater_limit_arr[1]<number & number<=quater_limit_arr[2]:
            index_quater = 2
        elif quater_limit_arr[2]<number & number<=quater_limit_arr[3]:
            index_quater = 3
        elif quater_limit_arr[3]<number & number<=quater_limit_arr[4]:
            index_quater = 4
        elif quater_limit_arr[4]<number & number<=quater_limit_arr[5]:
            index_quater = 5
        elif quater_limit_arr[5]<number & number<=quater_limit_arr[6]:
            index_quater = 6
        elif quater_limit_arr[6]<number & number<=quater_limit_arr[7]:
            index_quater = 7
        elif quater_limit_arr[7]<number & number<=quater_limit_arr[8]:
            index_quater = 8
        elif quater_limit_arr[8]<number & number<=quater_limit_arr[9]:
            index_quater = 9
        elif quater_limit_arr[9]<number & number<=quater_limit_arr[10]:
            index_quater = 10
        elif quater_limit_arr[10]<number & number<=quater_limit_arr[11]:
            index_quater = 11
        elif quater_limit_arr[11]<number & number<=quater_limit_arr[12]:
            index_quater = 12
        elif quater_limit_arr[12]<number & number<=quater_limit_arr[13]:
            index_quater = 13
        elif quater_limit_arr[13]<number & number<=quater_limit_arr[14]:
            index_quater = 14
        elif quater_limit_arr[14]<number & number<=quater_limit_arr[15]:
            index_quater = 15
        elif quater_limit_arr[15]<number & number<=quater_limit_arr[16]:
            index_quater = 16
        elif quater_limit_arr[16]<number & number<=quater_limit_arr[17]:
            index_quater = 17
        elif quater_limit_arr[17]<number & number<=quater_limit_arr[18]:
            index_quater = 18
        elif quater_limit_arr[18]<number & number<=quater_limit_arr[19]:
            index_quater = 19
        elif quater_limit_arr[19]<number & number<=quater_limit_arr[20]:
            index_quater = 20
        elif quater_limit_arr[20]<number & number<=quater_limit_arr[21]:
            index_quater = 21
        elif quater_limit_arr[21]<number & number<=quater_limit_arr[22]:
            index_quater = 22
        elif quater_limit_arr[22]<number & number<=quater_limit_arr[23]:
            index_quater = 23
        elif quater_limit_arr[23]<number & number<=quater_limit_arr[24]:
            index_quater = 24
        elif quater_limit_arr[24]<number & number<=quater_limit_arr[25]:
            index_quater = 25
        elif quater_limit_arr[25]<number & number<=quater_limit_arr[26]:
            index_quater = 26
        elif quater_limit_arr[26]<number & number<=quater_limit_arr[27]:
            index_quater = 27
        elif quater_limit_arr[27]<number & number<=quater_limit_arr[28]:
            index_quater = 28
        elif quater_limit_arr[28]<number & number<=quater_limit_arr[29]:
            index_quater = 29
        elif quater_limit_arr[29]<number & number<=quater_limit_arr[30]:
            index_quater = 30
        elif quater_limit_arr[30]<number & number<=quater_limit_arr[31]:
            index_quater = 31
        elif quater_limit_arr[31]<number & number<=quater_limit_arr[32]:
            index_quater = 32
        elif quater_limit_arr[32]<number & number<=quater_limit_arr[33]:
            index_quater = 33
        elif quater_limit_arr[33]<number & number<=quater_limit_arr[34]:
            index_quater = 34
        elif quater_limit_arr[34]<number & number<=quater_limit_arr[35]:
            index_quater = 35
        elif quater_limit_arr[35]<number & number<=quater_limit_arr[36]:
            index_quater = 36
        elif quater_limit_arr[36]<number & number<=quater_limit_arr[37]:
            index_quater = 37
        elif quater_limit_arr[37]<number & number<=quater_limit_arr[38]:
            index_quater = 38
        elif quater_limit_arr[38]<number & number<=quater_limit_arr[39]:
            index_quater = 39
        elif quater_limit_arr[39]<number & number<=quater_limit_arr[40]:
            index_quater = 40
        elif quater_limit_arr[40]<number & number<=quater_limit_arr[41]:
            index_quater = 41
        elif quater_limit_arr[41]<number & number<=quater_limit_arr[42]:
            index_quater = 42
        elif quater_limit_arr[42]<number & number<=quater_limit_arr[43]:
            index_quater = 43
        elif quater_limit_arr[43]<number & number<=quater_limit_arr[44]:
            index_quater = 44
        elif quater_limit_arr[44]<number & number<=quater_limit_arr[45]:
            index_quater = 45
        elif quater_limit_arr[45]<number & number<=quater_limit_arr[46]:
            index_quater = 46
        elif quater_limit_arr[46]<number & number<=quater_limit_arr[47]:
            index_quater = 47
        elif quater_limit_arr[47]<number & number<=quater_limit_arr[48]:
            index_quater = 48
        elif quater_limit_arr[48]<number & number<=quater_limit_arr[49]:
            index_quater = 49
        elif quater_limit_arr[50]<number & number<=quater_limit_arr[51]:
            index_quater = 50
        elif quater_limit_arr[51]<number & number<=quater_limit_arr[52]:
            index_quater = 51
        elif quater_limit_arr[52]<number & number<=quater_limit_arr[53]:
            index_quater = 52
        elif quater_limit_arr[53]<number & number<=quater_limit_arr[54]:
            index_quater = 53
        elif quater_limit_arr[54]<number & number<=quater_limit_arr[55]:
            index_quater = 54
        elif quater_limit_arr[55]<number & number<=quater_limit_arr[56]:
            index_quater = 55
        elif quater_limit_arr[56]<number & number<=quater_limit_arr[57]:
            index_quater = 56
        elif quater_limit_arr[57]<number & number<=quater_limit_arr[58]:
            index_quater = 57
        elif quater_limit_arr[58]<number & number<=quater_limit_arr[59]:
            index_quater = 58
        elif quater_limit_arr[59]<number & number<=quater_limit_arr[60]:
            index_quater = 59
        elif quater_limit_arr[60]<number & number<=quater_limit_arr[61]:
            index_quater = 60
        elif quater_limit_arr[61]<number & number<=quater_limit_arr[62]:
            index_quater = 61
        elif quater_limit_arr[62]<number & number<=quater_limit_arr[63]:
            index_quater = 62
        elif quater_limit_arr[63]<number & number<=quater_limit_arr[64]:
            index_quater = 63
        

        if legandre_symbol == 1:
            quater_legandre_counter_arr[index_quater] += 1
        elif legandre_symbol == -1:
            quater_legandre_counter_arr[index_quater] -= 1
        # result_arr.append(legandre_symbol) // для графика

    str_to_file = str(prime) + ',' + ','.join(str(num) for num in quater_legandre_counter_arr) + "\n"
    print(str_to_file)

if __name__ == "__main__":

    prime_numbers_array = []
    with open('primes.txt', 'r') as f:
        for eachLine in f:                                         
            line_array = list(map(int, eachLine.split()))
            prime_numbers_array += line_array 
    print(prime_numbers_array)                       
        
          
#     # step = 10000

#     # p_min = 0
#     # p_max = step#100 0000 0007
    

#     # prime_numbers_arr = []
#     # prime_numbers_arr_filtered = []

#     # prev_filename = ''

#     # while p_min<=step*step*100:
#     #     prime_numbers_arr = []
#     #     filename = "./crypto_py/primes_" + str(p_max) + ".csv"

#     #     if  prev_filename != '':
#     #         with open(prev_filename,'r') as firstfile, open(filename,'a') as secondfile:   
#     #             for line in firstfile:
#     #                 secondfile.write(line)
#     #     else:
#     #         f = open(filename, 'w')
#     #         str_to_file = "p, firs_quater, second_quater, third_quater, fourth_quater" + "\n"
#     #         f.write(str_to_file)
#     #         f.close()

#     #     f = open(filename, 'a')
#     #     for i in range(p_min, p_max):
#     #         if (isPrime(i)):
#     #             prime_numbers_arr.append(i)

#     #     for prime in prime_numbers_arr:

#     #         # result_arr = [] # выводим на график

#     #         quater_count_num = round(prime/4)   # делим по четвертям
#     #         quater_limit_arr = []               # границы четвертей
#     #         quater_legandre_counter_arr = []
#     #         for i in range(1, 4):
#     #             quater_limit_arr.append(i*quater_count_num)
#     #             quater_legandre_counter_arr.append(0)
#     #         quater_limit_arr.append(prime)
#     #         quater_legandre_counter_arr.append(0)

            
#     #         # считаем вычеты в диапазоне [0, р]
#     #         for number in range(0, prime):
#     #             legandre_symbol = calculateLegendre(number, prime)

#     #             index_quater = 0

#     #             if number<=quater_limit_arr[0]:
#     #                 index_quater = 0
#     #             elif quater_limit_arr[0]<number & number<=quater_limit_arr[1]:
#     #                 index_quater = 1
#     #             elif quater_limit_arr[1]<number & number<=quater_limit_arr[2]:
#     #                 index_quater = 2
#     #             elif quater_limit_arr[2]<number & number<=quater_limit_arr[3]:
#     #                 index_quater = 3

#     #             if legandre_symbol == 1:
#     #                 quater_legandre_counter_arr[index_quater] += 1
#     #             elif legandre_symbol == -1:
#     #                 quater_legandre_counter_arr[index_quater] -= 1
#     #             # result_arr.append(legandre_symbol) // для графика
        
#     #         if (quater_legandre_counter_arr[1] == 0 or quater_legandre_counter_arr[2] == 0):
#     #             prime_numbers_arr_filtered.append(prime)
#     #             str_to_file = str(prime) + ',' + ','.join(str(num) for num in quater_legandre_counter_arr) + "\n"
#     #             f.write(str_to_file)
#     #         # print(quater_legandre_counter_arr)
#     #     f.close()
#     #     prev_filename = filename

#     #     p_min += step
#     #     p_max += step

#     prime = 15619 #1000000007
#     # count_16(prime)

#     # result_arr = [82,20,26,14,20,-16,14,-6,32,-12,-8,0,6,20,-18,-20,20,20,-24,-4,-2,10,10,-30,8,-16,16,-22,-12,-26,-22,-80]
#     result_arr = [-32,0,-10,28,-2,10,-18,-10,12,-16,-8,-32,0,34,0,12,30,-2,-2,18,8,-16,2,38,4,16,-30,-6,-2,0,16,0,0,-16,2,0,6,28,-14,-2,-40,-2,16,-6,-20,2,0,-26,-14,0,-4,34,6,18,-12,10,18,-8,-2,-26,10,2,-2,0]

#     # cwd = os.getcwd()  # Get the current working directory (cwd)
#     # files = os.listdir(cwd)  # Get all the files in that directory
#     # print("Files in %r: %s" % (cwd, files))

#     plt.plot(range(1, 65), result_arr, '-ro')
#     plt.axis([0, 64, -82, 82])
#     plt.show()

# # p = 1000000007 #с

